#include "stdafx.h"
#include "function.h"

function & function::operator+= (Point p)
{
	if (n == 0){
		this->node = new Point;
		this->n = 1;
		this->node[0] = p;
	}
	else{
		Point *a = nullptr;
		int pos;
		try{
			a = new Point[n + 1];
		}
		catch (bad_alloc){
			throw(bad_alloc());
		}
		pos = find(n, p);
		if (p.x > node[n - 1].x)
			pos = n;
		if (p.x < node[0].x)
			pos = 0;
		for (int i = 0; i < pos; i++){
			a[i] = node[i];
		}
		a[pos] = p;
		for (int i = pos + 1; i < n + 1; i++){
			a[i] = node[i - 1];
		}
		delete[]node;
		node = nullptr;
		this->node = a;
		this->n++;
	}
	return *this;
}

double function::operator () (double x0) const{
	double k = 0, y = 0;
	if (n == 0)
		throw("function is empty");
	if ((x0 < node[0].x) || (x0 > node[n - 1].x))
		throw ("Can't compute the value");
	int m = 0;
	for (int i = 0; i < n; i++){
		if (x0 < node[i].x)
			break;
		m++;
	}
	y = node[m - 1].y + (x0 - node[m - 1].x)*(node[m].y - node[m - 1].y) / (node[m].x - node[m - 1].x);
	return y;
}

function::operator int() const{
	int k1 = 1;
	int k2 = 1;
	int k3 = 1;
	double buf;
	if (n == 0)
		throw("Function is empty");
	if (n == 1)
		return 0;
	for (int i = 1; i < n; i++){
		buf = node[i - 1].y;
		if (node[i].y == buf){
			k1++;
			if (k1 == n) return 0;
		}
		if (node[i].y > buf){
			k2++;
			if (k2 == n) return 1;
		}
		if (node[i].y < buf){
			k3++;
			if (k3 == n) return -1;
		}
	}
	return 2;
}

ostream & operator << (ostream &s,const function &f)
{
	for (int i = 0; i < f.n; i++){
		s << "(" << f.node[i].x << "," << f.node[i].y << ")" << endl;
	}
	std::cout << "" << std::endl;
	return s;
}

istream & operator >> (istream &s, function &f)
{
	Point p;
	s >> p.x;
    s >> p.y;
	if (s.good())
		f += p;
	else
		s.setstate(ios::failbit);
	return s;
}

function::function()
{
	this->n = 0;
	this->node = nullptr;
}

function::function(const Point &p){
	try{
		this->node = new Point;
	}
	catch (bad_alloc){
		delete[]node;
		node = nullptr;
		this->n = 0;
		throw(bad_alloc());
	}
	this->n = 1;
	this->node[0] = p;
}

function::function(const int n, const Point *&p){
	// throw n!= number of elements
	try{
		this->node = new Point[n];
	}
	catch(bad_alloc) {
		delete[]node;
		node = nullptr;
		this->n = 0;
		throw(bad_alloc());
	}
	this->n = n;
	for (int i = 0; i < n; i++)
		this->node[i] = p[i];
	sort(0, n - 1);
}

function::function(const function &f)
{
	n = f.n;
	node = NULL;
	if (n){
		node = new Point[n];
		for (int i = 0; i < n; i++){
			node[i] = f.node[i];
		}
	}
}

function & function::operator = (function && f)
{

	int tn = n;
	n = f.n;
	f.n = tn;
	Point *tptr = node;
	node = f.node;
	f.node = tptr;
	return *this;
}

function & function::operator = (const function& f)
{
	if (this != &f) { // проверка a = a
		delete[] node;
		node = NULL;
		if ((n = f.n) != 0) {
			try{
				node = new Point[n];
			}
			catch (bad_alloc) {
				delete[]node;
				node = nullptr;
				this->n = 0;
				throw(bad_alloc());
			}
			for (int i = 0; i < n; i++){
				node[i] = f.node[i];
			}
		}
	}
	return *this;
}

int function::getN() const{
	return n;
}

Point* function::getarray() const{
	return node;
}

int function::find(int n, const Point &p) const{
	int l = 0;
	int r = n + 1;
	int m;
	while (l<r - 1){
		m = (l + r) / 2;
		if (node[m].x < p.x)
			l = m;
		else
			r = m;
	}
	return r;
}

Point function::getarray(int pos) const{
	return node[pos];
}

void function::sort(int k, int n){
	int i, j;
	Point m, buf;
	i = k;
	j = n;
	m.x = node[(i + j) / 2].x;
	do {
		while (node[i].x < m.x) i++;
		while (node[j].x > m.x) j--;
		if (i <= j) {
			buf = node[i];
			node[i] = node[j];
			node[j] = buf;
			i++;
			j--;
		}
	} while (i <= j);

	if (k<j) sort(k, j);
	if (i<n) sort(i, n);
}

double function::min() const{
	double min;
	if (n == 0)
		throw("There are no elements");
	if (n == 1)
		return node[0].y;
	min = node[0].y;
	for (int i = 0; i < n; i++)
		if (node[i].y < min)
			min = node[i].y;
	return min;
}

double function::max() const{
	double max;
	if (n == 0)
		throw("There are no elements");
	if (n == 1)
		return node[0].y;
	max = node[0].y;
	for (int i = 1; i < n; i++)
		if (node[i].y > max)
			max = node[i].y;
	return max;
}

function::~function()
{
	delete[]node;
}

