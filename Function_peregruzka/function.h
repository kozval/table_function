#pragma once
#include <iostream>
using namespace std;
struct Point{
	double x;
	double y;
};
class function
{
private:
	Point *node;
	int n;
	void sort(int, int);
public:
	function();
	function(const Point&);
	function(const int, const Point*&);
	function(const function&);
	function & function::operator = (function &&);
	function & function::operator = (const function& );
	int getN() const;
	Point* getarray() const;
	Point getarray(int) const;
	function & operator += (Point);
	double  operator () (double) const;
	operator int() const;
	friend ostream & operator << (ostream &,const function &);
	friend istream & operator >> (istream &, function &);
	int find(int, const Point&) const;
	double min() const;
	double max() const;
	virtual~function();
};

