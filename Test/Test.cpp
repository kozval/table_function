// Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "..\Function_peregruzka\function.h"
#include "gtest\gtest.h"
#include <iostream>
#include <algorithm>

int r_number(){
	return  rand() % 20 + 1;
}

Point *r_array(const int n){
	Point *p0 = nullptr;
	p0 = new Point[n];
	for (int i = 0; i < n; i++){
		p0[i].x = (double)(rand() % 10000) / (double)10000 + (double)(rand() % 10000) *pow(-1, (rand() % 2) + 1);
		p0[i].y = (double)(rand() % 10000) / (double)10000 + (double)(rand() % 10000) *pow(-1, (rand() % 2) + 1);
	}

	std::sort(p0, p0 + n, [](const Point& a, const Point& b) {
		return a.x < b.x;
	});
	return p0;
}

Point *r_array0(const int n){
	Point *p0 = nullptr;
	p0 = new Point[n];
	for (int i = 0; i < n; i++){
		p0[i].x = i;
		p0[i].y = i;
	}
	return p0;
}

Point *r_array1(const int n){
	Point *p0 = nullptr;
	p0 = new Point[n];
	for (int i = 0; i < n; i++){
		p0[i].x = i;
		p0[i].y = -i;
	}
	return p0;
}

Point *r_array2(const int n){
	Point *p0 = nullptr;
	p0 = new Point[n];
	for (int i = 0; i < n; i++){
		p0[i].x = i;
		p0[i].y = pow(-i, i);
	}
	return p0;
}

Point *r_array3(const int n){
	Point *p0 = nullptr;
	p0 = new Point[n];
	for (int i = 0; i < n; i++){
		p0[i].x = i;
		p0[i].y = 1.26;
	}
	return p0;
}


TEST(Constructor, DefaultConstructor)
{
	function f;
	EXPECT_DOUBLE_EQ(0, f.getN());
}

TEST(Constructor, InitPoint)
{
	Point p0;
	p0.x = 1.2;
	p0.y = -0.8;

	function f(p0);
	EXPECT_DOUBLE_EQ(1, f.getN());

	Point *p = nullptr;
	p = f.getarray();
	EXPECT_DOUBLE_EQ(1.2, p->x);
	EXPECT_DOUBLE_EQ(-0.8, p->y);
}

TEST(Constructor, InitPointandNumber)
{
	srand(time(NULL));
	for (int k = 0; k < 100; k++){
		const Point *p0 = nullptr;
		int n = r_number();
		p0 = r_array(n);
		Point *p = nullptr;
		function f(n, p0);
		EXPECT_DOUBLE_EQ(n, f.getN());
		p = f.getarray();
		for (int i = 0; i < n; i++){
			EXPECT_DOUBLE_EQ(p0->x, p->x);
			EXPECT_DOUBLE_EQ(p0->y, p->y);
			p++;
			p0++;
		}
	}
}

TEST(Adding, Adding_operator){
	srand(time(NULL));
	for (int k = 0; k < 1000; k++){
		const Point *p0 = nullptr;
		Point *p1 = nullptr;
		int n = r_number();
		p0 = r_array(n);
		p1 = new Point[n + 1];
		for (int i = 0; i < n; i++){
			p1[i].x = p0[i].x;
			p1[i].y = p0[i].y;
		}
		function f(n, p0);
		Point add_me;
		Point *p = nullptr;
		add_me.x = (double)(rand() % 10000) / (double)10000 + (double)(rand() % 10000) *pow(-1, (rand() % 2) + 1);
		add_me.y = (double)(rand() % 10000) / (double)10000 + (double)(rand() % 10000) *pow(-1, (rand() % 2) + 1);
		f += add_me;
		p1[n] = add_me;
		std::sort(p1, p1 + n + 1, [](const Point& a, const Point& b) {
			return a.x < b.x;
		});
		EXPECT_DOUBLE_EQ(n + 1, f.getN());
		p = f.getarray();
		for (int i = 0; i < n; i++){
			EXPECT_DOUBLE_EQ(p1->x, p->x);
			EXPECT_DOUBLE_EQ(p1->y, p->y);
			p++;
			p1++;
		}
	}
}

TEST(Constructor, Copy){
	const Point *p0 = nullptr;
	const Point *p1 = nullptr;
	const Point *p2 = nullptr;
	int n = r_number();
	p0 = r_array(n);
	function f(n, p0);
	function b(f);
	p1 = f.getarray();
	p2 = b.getarray();
	for (int i = 0; i < n; i++){
		EXPECT_DOUBLE_EQ(p1->x, p2->x);
		EXPECT_DOUBLE_EQ(p1->y, p2->y);
		p1++;
		p2++;
	}
}

TEST(Init, Reinit_operetor){
	const Point *p0 = nullptr;
	const Point *p1 = nullptr;
	const Point *p2 = nullptr;
	int n = r_number();
	p0 = r_array(n);
	function f(n, p0);
	function a = f;
	p1 = f.getarray();
	p2 = a.getarray();
	for (int i = 0; i < n; i++){
		EXPECT_DOUBLE_EQ(p1->x, p2->x);
		EXPECT_DOUBLE_EQ(p1->y, p2->y);
		p1++;
		p2++;
	}
}

TEST(Calculations, Minimum)
{
	const Point *p0 = nullptr;
	srand(time(NULL));
	for (int i = 0; i < 10; i++){
		int n = r_number();
		p0 = r_array(n);
		function f(n, p0);
		double m;
		m = p0[0].y;
		for (int i = 1; i < n; i++){
			if (p0[i].y < m)
				m = p0[i].y;
		}
		EXPECT_DOUBLE_EQ(m, f.min());
	}
}

TEST(Calculations, Maximum)
{
	const Point *p0 = nullptr;
	srand(time(NULL));
	for (int i = 0; i < 10; i++){
		int n = r_number();
		p0 = r_array(n);
		function f(n, p0);

		double m;
		m = p0[0].y;
		for (int i = 1; i < n; i++){
			if (p0[i].y > m)
				m = p0[i].y;
		}
		EXPECT_DOUBLE_EQ(m, f.max());
	}
}

TEST(Calculations, Calculationthrow)
{
	function f;
	EXPECT_ANY_THROW(f.min());
	EXPECT_ANY_THROW(f.max());
}

TEST(Calculations, Type_operator)
{
	const Point *p0 = nullptr;
	const Point *p1 = nullptr;
	const Point *p2 = nullptr;
	const Point *p3 = nullptr;
	int n = 5;
	p0 = r_array0(n);
	p1 = r_array1(n);
	p2 = r_array2(n);
	p3 = r_array3(n);
	function f0(n, p0);
	function f1(n, p1);
	function f2(n, p2);
	function f3(n, p3);
	EXPECT_EQ(1, (int)f0);
	EXPECT_EQ(-1, (int)f1);
	EXPECT_EQ(2, (int)f2);
	EXPECT_EQ(0, (int)f3);
}

TEST(Calculations, Value_at_x0_operetor){
	const Point *p0 = nullptr;
	const Point *p1 = nullptr;
	const Point *p2 = nullptr;
	const Point *p3 = nullptr;
	int n = 5;
	p0 = r_array0(n);
	p1 = r_array1(n);
	p2 = r_array2(n);
	p3 = r_array3(n);
	function f0(n, p0);
	function f1(n, p1);
	function f2(n, p2);
	function f3(n, p3);
	EXPECT_DOUBLE_EQ(2, f0(2));
	EXPECT_DOUBLE_EQ(0.5, f0(0.5));
	EXPECT_ANY_THROW(f0(-1));
	EXPECT_DOUBLE_EQ(-2, f1(2));
	EXPECT_DOUBLE_EQ(-1.88, f1(1.88));
	EXPECT_ANY_THROW(f1(-1));
	EXPECT_DOUBLE_EQ(-11.5, f2(2.5));
	EXPECT_DOUBLE_EQ(256, f2(4));
	EXPECT_ANY_THROW(f2(-1));
	EXPECT_DOUBLE_EQ(1.26, f3(2));
	EXPECT_DOUBLE_EQ(1.26, f3(4));
	EXPECT_ANY_THROW(f3(5));
}

TEST(Calculations, Value_at_x0_operator_y_kx){
	const Point *p0 = nullptr;
	for (int i = 0; i < 100; i++){
		p0 = r_array(2);
		function f0(2, p0);
		double k = (f0.getarray(1).y - f0.getarray(0).y) / (f0.getarray(1).x - f0.getarray(0).x);
		double b = f0.getarray(0).y - f0.getarray(0).x * k;
		int x = f0.getarray(1).x - f0.getarray(0).x;
		for (int j = 0; j < 100; j++){
			double x0 = f0.getarray(0).x + rand() % x;
			EXPECT_NEAR(k*x0 + b, f0(x0), 0.00001);
		}
	}
}

TEST(A1, A12){
	function f;
	Point p;
	p.x = 1;
	p.y = 1;
	Point p2;
	p2.x = 2;
	p2.y = 2;
	f += p;
	cout << f << endl;
	f += p2;
	cout << f << endl;

}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;
}


